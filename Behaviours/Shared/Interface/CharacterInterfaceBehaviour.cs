﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using JTOE;

public class CharacterInterfaceBehaviour : MonoBehaviour
{
	private SceneManager _sceneManager;
	private PlayerManager _playerManager;

	public Canvas jamesCanvas;
	public Canvas elianaCanvas;
	public Button btnSwapPlayers;
	public Button btnTieClosests;

	// Use this for initialization
	void Start ()
	{
		_sceneManager = new SceneManager ();
		_playerManager = new PlayerManager ();

		btnSwapPlayers
			.onClick
			.AddListener (() => SwapPlayers_Clicked());

		btnTieClosests
			.onClick
			.AddListener (() => TiePlayers_Clicked ());
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	/// <summary>
	/// Swaps the players.
	/// </summary>
	public void SwapPlayers_Clicked()
	{
		_playerManager.SwitchCurrentPlayer ();
		_sceneManager.SwitchInteractiveObjects ();
		SwapBackgroundLayers ();
	}

	/// <summary>
	/// Ties the players.
	/// </summary>
	public void TiePlayers_Clicked()
	{
		_playerManager.TieClosests ();
	}

	private void SwapBackgroundLayers()
	{
		if (_sceneManager.CurrentScene is ISwappableScene) {
			int temp = jamesCanvas.sortingOrder;
			jamesCanvas.sortingOrder = elianaCanvas.sortingOrder;
			elianaCanvas.sortingOrder = temp;
		}
	}
}