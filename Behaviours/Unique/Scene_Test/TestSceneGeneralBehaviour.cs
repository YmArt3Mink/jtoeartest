﻿using UnityEngine;
using JTOE;
using System.Collections;
using System.Collections.Generic;

public class TestSceneGeneralBehaviour : MonoBehaviour
{
	private ICollection<AbstractPlayer> players;

	// Use this for initialization
	void Start ()
	{
		var sceneInitializer = new TestSceneInitializer ();
		var playerEntry = new PlayerSceneryEntry ();
		sceneInitializer.InitPlayerData ( playerEntry );
		UpdatePlayerView ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	private void UpdatePlayerView()
	{

	}
}

