﻿using System;

namespace JTOE
{
	public interface ISwappableScene
	{
		void Swap ();
	}
}

