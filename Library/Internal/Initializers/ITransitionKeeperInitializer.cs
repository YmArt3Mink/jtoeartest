﻿using System;

namespace JTOE
{
	public interface ITransitionKeeperInitializer
	{
		void Init ( ITransitionKeeper tKeeper );
	}
}

