﻿using System;

namespace JTOE
{
	public interface ISceneInitializer
	{
		void InitPlayerData ( PlayerSceneryEntry playerData );
		void InitSceneData  ( /* ENTRY */ );
	}
}

