﻿using System;
using System.Collections.Generic;

namespace JTOE
{
	/// <summary>
	/// Scene transitions' dictionary
	/// </summary>
	public class SceneTransitionsStruct : ITransitionKeeper
	{
		IDictionary<TransitionKeeperEntry, 
			TransitionKeeperEntry> transitions;

		public SceneTransitionsStruct ()
		{
			transitions = new Dictionary<TransitionKeeperEntry, TransitionKeeperEntry> ();
		}

		public void setTransition ( TransitionKeeperEntry source, TransitionKeeperEntry dest )
		{
			transitions.Add ( source, dest );
		}
				
		public TransitionKeeperEntry getTransitionFrom( TransitionKeeperEntry source )
		{
			return null;
		}

		public ICollection<TransitionKeeperEntry> getTransitionsTo	( TransitionKeeperEntry dest )
		{
			return null;
		}
	}
}

