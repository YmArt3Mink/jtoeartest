﻿using System;
using System.Collections.Generic;

namespace JTOE
{
	public class AbstractScene
	{
		protected int sceneId;
		protected string sceneDataFilename;

		/// <summary>
		/// Constructor is protected for the exemplar creating disallowment (abstract class emulation)
		/// </summary>
		protected AbstractScene()
		{
		}
	}
}

