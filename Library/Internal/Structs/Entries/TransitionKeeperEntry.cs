using System;
using System.Collections.Generic;

namespace JTOE
{
	/// <summary>
	/// TransitionKeeper's entry struct
	/// </summary>
	public class TransitionKeeperEntry
	{
		/// <summary>
		/// KeyValuePair<String, int> ~= KeyValuePair <SceneName, TransistorId>
		/// 'TransistorId' is the ID of an examinable object enables the process of transition between the scenes (if entry is source)
		/// or the receiver of transition process (if entry is destination)
		/// </summary>
		private KeyValuePair<String, int> entry;
		
		public TransitionKeeperEntry ( KeyValuePair<String, int> data )
		{
			entry = data;
		}
		
		public String getSceneName()
		{
			return entry.Key;
		}
		
		public int getTransistorId()
		{
			return entry.Value;
		}
	}
}

