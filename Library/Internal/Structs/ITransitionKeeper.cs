using System;
using System.Collections.Generic;

namespace JTOE
{
	/// <summary>
	/// Transition dictionary interface
	/// </summary>
	public interface ITransitionKeeper
	{
		void setTransition ( TransitionKeeperEntry source, TransitionKeeperEntry dest );
		
		TransitionKeeperEntry 							getTransitionFrom	( TransitionKeeperEntry source );
		ICollection<TransitionKeeperEntry>  getTransitionsTo	( TransitionKeeperEntry   dest );
	}
}

