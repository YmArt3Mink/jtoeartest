﻿using System;

namespace JTOE
{
	public interface ILoader
	{
		// <result>
		// Either the loading has succeeded or not
		// </result>
		bool Load( ILoadable loadable );
	}
}

