﻿using System;

namespace JTOE
{
	public interface ILoadable
	{
		void Visit ( ILoader loader );
	}
}

