﻿using System;

namespace JTOE
{
	/// <summary>
	/// Ingame progress data struct to save
	/// </summary>
	public class ProgressDataStruct : IWritable, ILoadable
	{
		public ProgressDataStruct ()
		{
		}
		
		public void Visit( ILoader loader )
		{
		}

		public void Visit( IWriter writer )
		{
		}
	}
}

