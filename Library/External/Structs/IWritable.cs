﻿using System;

namespace JTOE
{
	public interface IWritable
	{
		void Visit( IWriter writer );
	}
}

