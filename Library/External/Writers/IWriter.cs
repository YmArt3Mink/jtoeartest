﻿using System;

namespace JTOE
{
	public interface IWriter
	{
		bool Write ( IWritable writable );
	}
}

