﻿using System;

namespace JTOE
{
	public interface ICallable
	{
		/// <summary>
		/// Activate a process to call (pull) another player
		/// </summary>
		/// <param name="player">Being called Player.</param>
		void Call( AbstractPlayer player );
	}
}

