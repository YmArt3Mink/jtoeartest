﻿using System;

namespace JTOE
{
	public interface IInteractiveObjectVisitor
	{
		/// <summary>
		/// Visit methods to examine an InteraciveObjects
		/// </summary>
		void Visit ( ExecutableObject obj );
		void Visit ( TakableObject obj );
		void Visit ( PullableObject obj );
		void Visit ( ClimbableObject obj ); 
	}
}

