﻿using System;
using UnityEngine;

namespace JTOE
{
	public class AbstractPlayer : ICallable, ITiable, IInteractiveObjectVisitor
	{
		protected Vector2 			position;
		public PlayerSpriteState	animationState { get; protected set; }

		public int 							sceneId { get; set; }
		public ITiable						tiedWith;

		/// <summary>
		/// Activate a process to call (pull) another player
		/// in order to pull them together
		/// </summary>
		/// <param name="player">Being called Player.</param>
		public void Call( AbstractPlayer player )
		{
			// TODO : Insert the other player calling code
		}

		/// <summary>
		/// Ties the players' hands together
		/// </summary>
		/// <param name="player">Being tied Player.</param>
		public void Tie( AbstractPlayer player )
		{
			// TODO : Insert tiement code
		}

		/// <summary>
		/// Visits the IExaminable descendant
		/// </summary>
		/// <param name="obj">Executable object.</param>
		public void Visit( ExecutableObject obj )
		{
			// _blank
		}

		public void Visit ( TakableObject obj )
		{
			// TODO : Insert the inventory adding code
		}

		public void Visit ( PullableObject obj )
		{
			// _blank
		}

		public void Visit ( ClimbableObject obj )
		{
			// _blank
		}
	}
}

