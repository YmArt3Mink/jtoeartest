﻿using System;

namespace JTOE
{
	public interface ITiable
	{
		/// <summary>
		/// Ties the player with another one
		/// </summary>
		/// <param name="player">Being tied Player.</param>
		void Tie( AbstractPlayer player );
	}
}

