﻿using System;

namespace JTOE
{
	/// <summary>
	/// Pushable/Pullable object.
	/// </summary>
	public class PullableObject : IExaminable
	{
		public PullableObject ()
		{
		}

		public void Accept( IInteractiveObjectVisitor player )
		{
			player.Visit ( this );
		}
	}
}

