﻿using System;

namespace JTOE
{
	public interface IExaminable
	{
		/// <summary>
		/// Accept the specified player to examine
		/// </summary>
		/// <param name="player">Player.</param>
		void Accept( IInteractiveObjectVisitor player );
	}
}

