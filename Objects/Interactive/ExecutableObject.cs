﻿using System;

namespace JTOE
{
	/// <summary>
	/// Executable object.
	/// Execute ~= Click and glimpse the consequences
	/// </summary>
	public class ExecutableObject : IExaminable
	{
		public ExecutableObject ()
		{
		}

		public void Accept( IInteractiveObjectVisitor player )
		{
			player.Visit ( this );
		}
	}
}

