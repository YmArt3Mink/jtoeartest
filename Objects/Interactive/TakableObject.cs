﻿using System;

namespace JTOE
{
	/// <summary>
	/// Takable object (i,e, item)
	/// </summary>
	public class TakableObject : IExaminable
	{
		public TakableObject ()
		{
		}

		public void Accept( IInteractiveObjectVisitor player )
		{
			player.Visit ( this );
		}
	}
}

