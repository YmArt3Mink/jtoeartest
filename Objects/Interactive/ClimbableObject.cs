﻿using System;

namespace JTOE
{
	public class ClimbableObject : IExaminable
	{
		public ClimbableObject ()
		{
		}

		public void Accept ( IInteractiveObjectVisitor player )
		{
			player.Visit (this);
		}
	}
}

