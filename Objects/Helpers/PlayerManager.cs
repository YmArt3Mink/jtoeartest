﻿using System;
using System.Collections.Generic;

namespace JTOE
{
	/// <summary>
	/// Playable characters' controller
	/// </summary>
	public class PlayerManager
	{
		/// <summary>
		/// Gets the players.
		/// </summary>
		/// <value>The players.</value>
		public ICollection<AbstractPlayer> Players {
			get;
			private set;
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="JTOE.PlayerManager"/> are tied.
		/// </summary>
		/// <value><c>true</c> if are tied; otherwise, <c>false</c>.</value>
		public bool AreTied {
			get;
			private set;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="JTOE.PlayerManager"/> class.
		/// </summary>
		public PlayerManager ()
		{
		}

		/// <summary>
		/// AbstractPlayer.Tie() wrapper
		/// </summary>
		public void TieClosests()
		{
			Players [0].Tie (Players [1]);
			AreTied = true;
		}

		/// <summary>
		/// Untie the players.
		/// </summary>
		public void Untie()
		{

		}

		/// <summary>
		/// Switchs the current player.
		/// </summary>
		public void SwitchCurrentPlayer()
		{
			
		}
	}
}
