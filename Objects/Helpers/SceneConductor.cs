﻿using System;

namespace JTOE
{
	/// <summary>
	/// Handles transitions between the scenes
	/// </summary>
	public class SceneConductor
	{
		private ITransitionKeeper transitionDict;
		
		public SceneConductor ()
		{
			transitionDict = new SceneTransitionsStruct();
			ITransitionKeeperInitializer initializer = new TestTransitionKeeperInitializer();
			initializer.Init( transitionDict );
		}

		public void Transit( AbstractPlayer player, TransitionKeeperEntry dest ) 
		{
			
		}
	}
}

