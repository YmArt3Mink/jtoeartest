﻿using System;

namespace JTOE
{
	/// <summary>
	/// Global scene manager.
	/// Provides the scenes' info.
	/// </summary>
	/// <remarks>
	/// Every scene has 2 or more different assets of interactive objects
	/// (e.g. James has one set and Eliana has another one).
	/// When player swaps the character SceneManager should swap
	/// interactive objects' set too.
	/// </remarks>
	public class SceneManager
	{
		/// <summary>
		/// CurrentScene cache.
		/// </summary>
		private AbstractScene _curSceneValue;

		/// <summary>
		/// Gets the current scene.
		/// </summary>
		/// <value>The current scene.</value>
		public AbstractScene CurrentScene {
			get {
				_curSceneValue = _curSceneValue ?? LoadCurrentScene (SettingsManager.SettingsFilename);
				return _curSceneValue;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="JTOE.SceneManager"/> class.
		/// </summary>
		public SceneManager ()
		{
			_curSceneValue = null;
		}

		/// <summary>
		/// Switchs the interactive objects of the scene
		/// </summary>
		public void SwitchInteractiveObjects ()
		{

		}

		/// <summary>
		/// Loads the current scene.
		/// </summary>
		/// <returns>The current scene.</returns>
		/// <param name="settingsFilename">Settings filename.</param>
		private AbstractScene LoadCurrentScene(String settingsFilename)
		{
			return null;
		}
	}
}

